# Algorithms

Jupyter Notebook Practice Problems for Algorithms.

## Installation

1. Install Anaconda, python == 3.7

2. `conda env create -f environment.yml`

### Quick Start

`chmod a+x start_alg.sh`

`./start_alg.sh`

### Save Progress

1. (First Time) git checkout -b [BRANCH_NAME]
2. git commit -a "COMMIT MESSAGE"
3. git push origin [BRANCH_NAME]

### For new problem releases

1. git pull origin master

## Table of Contents

1. Linked Lists
2. Trees
   - Binary Search Trees
3. Stacks
4. Queues

## Types of Leetcode Problems

1. Sliding Window
2. Pattern: Subsets

   - Subsets
   - Subsets with Duplicates
   - Permutations
   - String Permutations by changing case
   - Balanced Parentheses
   - Unique Generalized Abbreviation

3. Top K Elements
   - Top K Numbers
   - Kth Smallest Number
   - K Closest Point to the Origin
   - Connect Ropes
   - Top K Frequent Numbers
   - Frequency Sort
   - Kth Largest Number in a Stream
   - K Closest Numbers
   - Maximum Distinct Elements
   - Sum of Elements
   - Rearrange String
4. K-way merge
   - Merge K Sorted Lists
   - Kth Smallest Number in M Sorted Lists
   - Kth Smallest Number in a Sorted Matrix
   - Smallest Number Range

## How to study algorithms?

Algorithms can be found in both mathematics and computer science. It is a set of procedure and rules aims to solve a problem with given conditions.

The goal of algorithm is to solve problems. So, we need to think about three things in algorithms:

1. What problem are we solving?
2. How do we solve it?

   - Correctness
   - Time Complexity
   - Space Complexity

3. How can we improve it?

## Interesting Problems

How do we connect these theories to real applications?

**When studying problem solving**, think

1. Why use linked list instead of Arraylist
2. Why Queue instead of Stack?

_Take Notes on_

1. Common Error
   - Syntatical
   - Runtime
2. Language and New Syntax
3. Algorithm Writeups
