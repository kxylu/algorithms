import ipycytoscape
from nose.tools import assert_equal
import json
with open("Trees/trees_style.json") as fp:
    initial_style = json.load(fp)

class TreeNode:
    def __init__(self, data, N=4): # Let N be the number of children on each level
        self.data = data
        self.children = [None for _ in range(N)]
        self.N = N
    
    @property
    def _is_leaf(self):
        return all([c is None for c in self.children])

    @staticmethod
    def preorder_traversal(root):
        result = []
        def helper(node, result):
            if node is None:
                return
            result.append(str(node.data))
            for c in node.children:
                helper(c, result)
        helper(root, result)
        return result

    @staticmethod
    def level_order_traversal(root):
        result = []
        queue = [root]
        while len(queue) > 0:
            curr_node = queue.pop(0)
            result.append(str(curr_node.data))
            for c in curr_node.children:
                if c is not None:
                    queue.append(c)
        return result

    @staticmethod
    def postorder_traversal(root):
        result = []
        def helper(node, result):
            if node is None:
                return
            for c in node.children:
                helper(c, result)
            result.append(str(node.data))
        helper(root, result)
        return result

    @staticmethod
    def is_leaf(node):
        if not node:
            return False
        return all([c is None for c in node.children])

    @staticmethod
    def num_leaves(root):
        if root is None:
            return 0
        if root._is_leaf:
            return 1
        return sum([TreeNode.num_leaves(c) for c in root.children])


    @staticmethod
    def num_nodes(root):
        if root is None:
            return 0
        return sum([TreeNode.num_nodes(c) for c in root.children]) + 1
    
    @staticmethod
    def initiate_cytospade():
        cytoscapeobj = ipycytoscape.CytoscapeWidget()
        cytoscapeobj.set_style(initial_style)
        return cytoscapeobj

    @property
    def width(self):
        ### BEGIN SOLUTION
        queue = [self]
        nxt_lvl = []
        width = 1
        while len(queue) > 0 or len(nxt_lvl) > 0:
            curr_node = queue.pop(-1)
            nxt_lvl.extend([c for c in curr_node.children if c is not None])
            if len(queue) == 0:
                width = max(width, len(nxt_lvl))
                queue.extend(nxt_lvl)
                nxt_lvl = []
        return width
        ### END SOLUTION

    @staticmethod
    def print_tree(root, return_mapping=False):
        mapping = {}
        def retrieve_edges_nodes(node, edges, nodes, _id="n1"):
            if not node:
                return
            nodes[_id] = str(node.data)
            mapping[_id] = node
            for c in node.children:
                if c:
                    key = "n" + str(c.data)
                    while key in nodes:
                        key += "1"
                    edges.append({"source":_id, "target": key})
                    retrieve_edges_nodes(c, edges, nodes, key)
        edges = []
        nodes = {}
        retrieve_edges_nodes(root, edges, nodes)

        data = {"nodes":[{"data": {"id": _id, "name": nodes[_id]}}for _id in nodes],
                  "edges":[{"data":e} for e in edges]}
        cytoscapeobj = TreeNode.initiate_cytospade()
        cytoscapeobj.graph.add_graph_from_json(data) 
        if return_mapping:
            return cytoscapeobj, mapping
        return cytoscapeobj
            

    def set_children(self, children):
        new_c = len(children)
        c = len(self.children)
        if new_c - c < 0:
            children.extend([None for _ in range(c - new_c)])
        self.children = children

    def insert(self, data):
        for i in range(len(self.children)):
            if not self.children[i]:
                self.children[i] = TreeNode(data)
                return True
        for c in self.children:
            if c.insert(data):
                return True
        return False

    @property
    def height(self):
        return max([1 + c.height for c in self.children if c] + [1])

    @staticmethod
    def copy_tree(tree):
        def helper(node):
            new_node = TreeNode(node.data, node.N)
            new_node.set_children([helper(c) for c in node.children])
            return new_node
        return helper(tree)

    @staticmethod
    def create_tree(pairing, N=-1):
        if len(pairing) == 0:
            return None
        if N == -1:
            N = max([len(k[-1]) for k in pairing])
        data, children_data = pairing.pop(0)
        root = TreeNode(data, N)
        children = [TreeNode(c_data, N) for c_data in children_data if c_data is not None]
        root.set_children(children)
        queue = []
        queue.extend(children)
        while len(pairing) > 0:
            new_data, new_c = pairing[0]
            while len(queue) > 0:
                node = queue.pop(0)
                if node and node.data == new_data:
                    pairing.pop(0)
                    new_children = [TreeNode(c_data, N) for c_data in new_c if c_data]
                    node.set_children(new_children)
                    queue.extend(new_children)
                    break
        return root
                
class BTNode(TreeNode):
    def __init__(self, data):
        super(BTNode, self).__init__(data, 2)
        
    def is_balanced(self):
        return
    
    def is_full(self):
        return

class BSTNode(BTNode):
    def __init__(self, data):
        super(BSTNode, self).__init__(data)
        
    @property
    def left(self):
        if len(self.children) > 0:
            return self.children[0]
        return None
    
    @property
    def right(self):
        if len(self.children) > 1:
            return self.children[1]
        return None

    def insert(self, data):
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.children[0] = Node(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.children[1] = Node(data)
                else:
                    self.right.insert(data)
            

##### SAMPLES OF TREES #####
t2 = [(1, [2, 3, 4]), (2, [5, 6, 7])] # A Complete Tree
t3 = [(1, [2, 3]), (2, [4, 5])] # A Complete Full Binary Tree
t4 = [(1, [2, None]), (2, [3, None]), (3, [4, None]), (4, [5, None])]

complete_tree = TreeNode.create_tree(t2)
cfb_tree = TreeNode.create_tree(t3)
ll_tree = TreeNode.create_tree(t4)

sample_trees = {
    "complete_tree": complete_tree,
    "cfb_tree": cfb_tree,
    "ll_tree": ll_tree
}

##### Testing Utils #####
def test_tree_nodes(node, test_func, correct_func):
    # Iterate Tree and catch incorrect node
    correct_lst = []
    actual_lst = []
    def tree_iter(node, func, result):
        if func(node):
            result.append(node)
        if node is None:
            return
        for c in node.children:
            tree_iter(c, func, result)
    tree_iter(node, correct_func, correct_lst)
    tree_iter(node, test_func, actual_lst)
    print("Expected Nodes: {}".format("|".join([str(node.data) for node in correct_lst])))
    print("Actual Nodes: {}".format("|".join([str(node.data) for node in actual_lst])))
    # Catch Assertion Error
    # Create Cytospade Objects
    expected, e_mapping = TreeNode.print_tree(node, True)
    actual, a_mapping = TreeNode.print_tree(node, True)
    # Actual Tree, Expected Tree
    for node in expected.graph.nodes:
        if node.data["id"] in e_mapping and e_mapping[node.data["id"]] in correct_lst:
            node.classes = " ".join(list(set(node.classes.split(" "))) + ["correct"])
    for node in actual.graph.nodes:
        if node.data["id"] in e_mapping and e_mapping[node.data["id"]] in correct_lst and node.data["id"] in a_mapping and a_mapping[node.data["id"]] in actual_lst:
            node.classes = " ".join(list(set(node.classes.split(" "))) + ["correct"])
        elif  (node.data["id"] in a_mapping and a_mapping[node.data["id"]] in actual_lst) and not  (node.data["id"] in e_mapping and e_mapping[node.data["id"]] in correct_lst):
            node.classes = " ".join(list(set(node.classes.split(" "))) + ["false_positive"])
        elif  not (node.data["id"] in a_mapping and a_mapping[node.data["id"]] in actual_lst) and  (node.data["id"] in e_mapping and e_mapping[node.data["id"]] in correct_lst):
            node.classes = " ".join(list(set(node.classes.split(" "))) + ["false_negative"])
    return expected, actual, correct_lst == actual_lst

def test_tree_values(node, test_func, correct_val):
    # test_func tests trees and returns a value
    assert_equal(test_func(node), correct_val)
    print(" The solution is correct: \n Expected {0}, Actual: {1}".format(correct_val, test_func(node)))
    return True

def test_tree_func(tree_name, test_func, correct_func, val_only=True):
    if tree_name not in sample_trees:
        raise Exception("Test Not Found {}".format(tree_name))
    source_tree = sample_trees[tree_name]
    copied_tree = TreeNode.copy_tree(source_tree)
    actual_tree = test_func(source_tree)
    expected_tree = correct_func(copied_tree)

    # For now we test val only
    assert_equal(is_same_tree(actual_tree, expected_tree))
    return

def is_same_tree(tree1, tree2, val_only=True):
    """Testing the result node refers to the 
    same node
    """
    def helper(node1, node2):
        if not node1 and not node2:
            return True
        if node1.data != node2.data:
            return False
        if val_only and node1 is not node2:
            return False
        return helper(node1.left, node2.left) and helper(node1.right, node2.right)
    return helper(tree1, tree2)


def test_traversal(traversal_function, root):
    return