#!/bin/sh
conda activate algorithms
conda env update --file environment.yml
jupyter nbextension enable --user --py nbgrader
jupyter notebook release
