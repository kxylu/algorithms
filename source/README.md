# Algorithms

This is a collection of algorithms practice problems.
Follow the table of contents for how this repository is directed.

## Installation

```

```

## Table of Content

[### Algorithms Overview](./01-Algorithms-Overview)

1. [How to study algorithms](./01-Algorithms-Overview/1-Algorithms-Overview.md)

2. Master Theorem

3. Time Analysis

4. Space Analysis

### Data Structures

1. Linked Lists

2. Stacks and Queues

3. Trees

4. Hash Tables

5. Heaps