import sys
from os.path import dirname, abspath, join
d = dirname(dirname(dirname(abspath(__file__))))
sys.path.append(join(d))

from lib.linked_list import LinkedList, LinkedListNode
from lib.test_utils import test_val, test_same_node
from nose.tools import assert_equal
### LINKED LIST UTILS ###
Node = LinkedListNode

# Empty Linked List
empty = LinkedList.create_ll([])

# One element linked list
one_element = LinkedList.create_ll([1])

# Linked List with duplication
dup = LinkedList.create_ll([1, 1, 2, 2, 2, 3, 4, 4])

# Ordered Linked List
ordered_ll = LinkedList.create_ll([1, 2, 3, 4, 5])

# Random Linked List
random_ll = LinkedList.create_ll([1, 4, 2, 6, 7, 3])

# ll_with_cycle
ll_with_cycle = LinkedList.create_ll([1, 8, 2, 6, 7, -1])
ll_with_cycle._tail.set_next(ll_with_cycle.head)
# ll_without_cycle
ll_no_cycle = LinkedList.create_ll([2, 4, 2, 6, 7, 3])

# Linked List with


def load_linked_lists():
    return {"empty": empty, "one": one_element, "dup": dup, "ordered_ll": ordered_ll, "random_ll": random_ll, "ll_with_cycle":ll_with_cycle, "ll_no_cycle":ll_no_cycle}

# TESTING LINKED LISTS


def is_linked_list(l1, l2):
    return

# TESTING TREES


def is_same_tree(t1, t2):
    return


def mutate_ll(should_mutate=False):
    def actual_decorator(func):
        def inner(*a, **kw):
            # usually, a is the linked list
            if isinstance(a[0], LinkedList):
                # Create a list to track linked list
                a[0].reset_flags()
                a[0].reset_index()
                ll = a[0].to_list()
                result = func(*a)
                no_mutate = a[0].match(ll)
                return result,  (should_mutate or no_mutate), should_mutate
        return inner
    return actual_decorator



### DEPRECATED
def test_ll_val(func, inputs):
    inputs, expected = inputs
    raw_ll = inputs[0]
    head = Node.create_ll(raw_ll)
    print("Testing Linked List: ")
    Node.print_ll(head)
    result = func(head, *inputs[1:])
    assert_equal(result, expected)
    print("Expected: {0}, Result: {1}".format(expected, result))
    print("\n")


def test_ll_node(func, inputs, val_only=True):
    """Testing the result node refers to the 
    same node
    """
    inputs, expected = inputs
    if expected is None:
        expected = Node.empty()
    raw_ll = inputs[0]
    head = Node.create_ll(raw_ll)
    print("Testing Linked List: ")
    Node.print_ll(head)
    result = func(head, *inputs[1:])
    print(result)
    print(expected)
    if val_only:
        if result:
            assert_equal(result.data, expected)
        else:
            assert_equal(result == expected, True)
    else:
        
        expected = _get_nth(head, expected)
        Node.print_ll(expected)
        assert_equal(result is expected, True)
    print("Expected: {0}, Result: {1}".format(expected, result))
    print("\n")

## DEPRECATED ###
def _get_nth(head, index):
    while head and index > 0:
        head = head.nxt
        index -= 1
    return head

def _same_ll(a, b, val_only):
    # Return True if the linked list is 
    # same linked list
    while a and b:
        if val_only:
            if a.data != b.data:
                return False
        else:
            if a is not b:
                return False
        a = a.nxt
        b = b.nxt
    
    return not a and not b

def test_same_ll(func, inputs, val_only=True):
    """Testing the result node refers to the 
    same node
    """
    inputs, _expected = inputs
    raw_ll = inputs[0]
    head = Node.create_ll(raw_ll)
    expected = Node.create_ll(_expected)

    print("Testing Linked List: ")
    Node.print_ll(head)
    result = func(head, *inputs[1:])

    print("Expected: ", end="    ")
    print(Node.print_ll(expected))
    print("Actual: ", end= "   ")
    print(Node.print_ll(result))
    print("\n\n")
    assert_equal(_same_ll(result, expected, val_only), True)
