import json
import ipycytoscape
from .misc_utils import Vertex, Edge
from IPython.display import display
from os.path import dirname, abspath, join
d = dirname(abspath(__file__))

with open(join(d, "styles/linked_list.json")) as fp:
    initial_style = json.load(fp)


def initiate_cytospade():
    cytoscapeobj = ipycytoscape.CytoscapeWidget()
    return cytoscapeobj

class DoublyLinkedList:
    def __init__(self):
        return


class LinkedListNode(Vertex):
    def __init__(self, data, nxt=None):
        super(LinkedListNode, self).__init__(index=-1, value=data)
        self._is_empty = False
        self._is_sentinel = False
        self._nxt_edge = Edge.empty_edge()
        self.flag = None
        self.data = data
        self._nxt = nxt
        if data is None and nxt is None:
            # Empty Node
            self._is_empty = True
        elif data is None:
            # Sentinel Node
            self._is_sentinel = True
            self._nxt_edge = Edge((self, nxt), val="->")
        else:
            self.set_next(nxt)

    def push(self, v):
        if isinstance(v, LinkedListNode):
            v.nxt = self
            return v
        else:
            return LinkedListNode(v, self)

    @property
    def has_style(self):
        return False

    def add_style(self):
        data = [e for e in initial_style if e["selector"] == "node"][0]
        # Modify data here
        new_data = {**data}
        new_data["selector"] = "node[id = '{}']".format(self.index)
        return new_data

    def __bool__(self):
        return not self.is_empty
    @property
    def nxt_edge(self):
        return self._nxt_edge

    def __getattribute__(self, name):
        if name == "nxt":
            return self._nxt
        return super().__getattribute__(name)
            
    def copy(self, with_nxt=True):
        if with_nxt:
            return LinkedListNode(self.data, self._nxt)

        return LinkedListNode(self.data)

    @property
    def is_empty(self):
        if self.data is not None:
            self._is_empty = False
        return self._is_empty

    @property
    def position(self):
        if self.index >= 0:
            y = 50
            x = 50 + self.index * 100
            if self.nxt.index < self.index:
                y -= 150
                x -= 100
            return {"x":x, "y": y}
        return {}

    @staticmethod
    def get_sentinel(head):
        """Sentinel Node is used

        """
        return LinkedListNode(data=None, nxt=head)
        
    def get_set(self):
        if self.data is None:
            return set()
        if self._nxt is None:
            return set([self.data])
        return set([self.data]).union(self._nxt.get_set())

    @staticmethod
    def create_ll(lst):
        node = LinkedListNode.empty()
        for v in lst[::-1]:
            node = LinkedListNode(v, node)
        return node

    @staticmethod
    def empty():
        return LinkedListNode(data=None, nxt=None)

    def __setattr__(self, name, value):

        if name == "nxt":
            if value is None:
                value = LinkedListNode.empty()
            self.set_next(value)
        else:
            super().__setattr__(name, value)

    @staticmethod
    def display(ll):
        cytospade_obj = initiate_cytospade()
        return



    def set_next_edge(self, nxt_edge):
        self._nxt_edge = nxt_edge
        self._nxt = nxt_edge.dst

    def set_next(self, nxt):
        if isinstance(nxt, LinkedListNode):
            self._nxt = nxt
            
        else:
            if not nxt:
                self._nxt = LinkedListNode.empty()
            else:
                self._nxt = LinkedListNode(nxt)
        self._nxt_edge = Edge((self, self._nxt), val="->")
        return self._nxt

    @staticmethod
    def print_ll(node)->None:
        print(LinkedList(node))
        return

class LinkedList:
    def __init__(self, head:LinkedListNode):
        self.head = head

        self._tail = self._get_tail(head)

    def _get_tail(self, head:LinkedListNode)->LinkedListNode:
        """Retrive the last node in a linked list.  Return EmptyNode if
        Linked List is empty

        Args:
            head (LinkedListNode): start of linked list

        Returns:
            LinkedListNode: end of linked list.
        """
        self._length = 0
        if not head:
            return head
        ptr = head
        while head.nxt:
            
            if ptr is head.nxt:
                return head
            head = head.nxt
            self._length += 1
            
        self._length += 1
        return head

    def push(self, v):
        self.head = self.head.push(v)

    def set_properties(self, d):
        return

    def __setattr__(self, name, value):
        if name == "head" and value is None:
            value = LinkedListNode.empty()
        return super().__setattr__(name, value)

    @staticmethod
    def reverse_ll(ll):
        s_head = ll.head
        if not s_head:
            return ll
        prev = s_head
        s_head = s_head.nxt
        prev.nxt = None
        while s_head:
            nxt = s_head.nxt
            s_head.nxt = prev
            prev = s_head
            s_head = nxt
        ll.head = prev
        return ll

    @staticmethod
    def reverse_k(ll, k):
        ### BEGIN SOLUTION
        length = ll.length
        i = 1
        while i < length:
            j = i + k - 1
            LinkedList.reverse_sublist(ll, i, min(j, length))
            i = j + 1
        ## END SOLUTION
        return ll

    @staticmethod
    def reverse_sublist(ll, s, f):
        ### BEGIN SOLUTION
        if s >= 2:
            head = ll[s-2]
            prev = head.nxt
        else:
            head = ll.head
            prev = head
        if not head or not head.nxt:
            return ll
        
        start = prev
        s_head = start.nxt
        i = 0
        f = min(f, ll.length)
        while i < (f - s):
            nxt = s_head.nxt
            s_head.nxt = prev
            prev = s_head
            s_head = nxt
            i += 1
        if s < 2:
            # reset head
            ll.head = prev
        else:
            head.nxt = prev
        start.nxt = s_head
        ### END SOLUTION
        return ll

    @staticmethod
    def contains_cycle(ll)->bool:
        head = ll.head.nxt
        ### BEGIN SOLUTION
        while head:
            if head is ll.head:
                return True
            head = head.nxt
        return False

    @staticmethod
    def add_curve(edge_id):
        data = [e for e in initial_style if e["selector"] == "edge"][0]
        # Modify data here
        new_data = {**data}
        # new_data["style"]["curve-style"] = "loop"
        new_data["selector"] = "edge[id = '{}']".format(edge_id)
        return new_data

    def pop(self):
        ptr = self.head
        if ptr.is_empty:
            return
        self.head = self.head.nxt
        return ptr

    @property
    def length(self):
        return self._length

    def __getitem__(self, i):
        if i >= self.length:
            return None
        ptr = self.head
        while i > 0:
            ptr = ptr.nxt
            i -= 1
        return ptr
        

    @property
    def tail(self):
        return self._get_tail(self.head)

    def _get_length(self):
        self._length = 0
        def cnt(node):
            self._length += 1
            return node
        self.map_ll(cnt)

    def copy(self):
        old_ptr = self.head
        new_ptr = self.head.copy(False)
        new_ll = LinkedList(new_ptr)
        self.reset_index()
        if new_ptr.is_empty:
            return new_ll

        index_to_new_node = {0:new_ptr}
        old_ptr.index = 0
        i = 0
        while old_ptr:
            if old_ptr.nxt and old_ptr.nxt.index in index_to_new_node:
                nxt = index_to_new_node[old_ptr.nxt.index]
                new_ptr.set_next(nxt)
                break
            new_ptr = new_ptr.set_next(old_ptr.nxt.copy(False))
            old_ptr = old_ptr.nxt
            i += 1
            index_to_new_node[i] = new_ptr
            old_ptr.index = i

            new_ll._tail = new_ptr
        new_ll._length = self._length
        self.reset_index()
        return new_ll

    def __eq__(self, b):
        if not isinstance(b, LinkedList):
            return False
        return self.to_list() == b.to_list()

    @staticmethod
    def get_cycle(ll):
        head = ll.head
        ### BEGIN SOLUTION
        while not head.is_empty:
            if head.nxt.flag:
                return head
            head.set_flag(True)
            head = head.nxt
        return None

    @staticmethod
    def get_middle(ll):
        ptr = ll.head
        fast = ptr
        slow = ptr
        ### BEGIN SOLUTION
        while fast and not fast.is_empty and fast.nxt and not fast.nxt.is_empty:
            slow = slow.nxt
            fast = fast.nxt.nxt
        return slow
        ### END SOLUTION

    def is_same_ll(self, b):
        if not isinstance(b, LinkedList):
            return False
        result = [b.head, True]
        def is_same(node):
            result[1] = result[0] is node
            result[0] = result[0].nxt

        self.map_ll(is_same)
        return result[1]

    def to_list(self):
        result = []

        def append(node):
            result.append(node)
            return node
        self.map_ll(append)
        return result

    def match(self, lst):
        i = [0, True]

        def test_match(node):
            if lst[i[0]] is not node:
                i[1] = False
            i[0] += 1
            return node
        return i[1]

    def map_ll(self, func):
        # Apply a function to every node on linked list
        ptr = self.head
        while ptr:
            if ptr.flag:
                break
            func(ptr)
            ptr.set_flag(True)
            ptr = ptr.nxt
        self.reset_flags()
        return self

    def append(self, data):
        self._length += 1
        if isinstance(data, LinkedListNode):
            if self._tail.is_empty:
                self._tail = data
                self.head = data
                return
        self._tail = self._tail.set_next(data)
        if self.head.is_empty or self.head.nxt.is_empty:
            self.head = self._tail


    @staticmethod
    def create_ll(lst):
        node = LinkedListNode.empty()
        for v in lst[::-1]:
            node = LinkedListNode(v, node)
        return LinkedList(node)

    def __str__(self):
        s = []

        def build_str(head):
            s.append("|{0}| {1} ".format(head.data, head.nxt_edge))
            return head
        self.map_ll(build_str)
        return "".join(s)

    def __repr__(self):
        s = []

        def build_str(head):
            s.append("|{0}|{1} ".format(head.data, head.nxt_edge))
            return head
        self.map_ll(build_str)
        return "".join(s)

    def reset_flags(self):
        i = 0
        head = self.head
        while i < self.length:
            head.reset_flag()
            head = head.nxt
            i += 1

    def reset_index(self):
        i = 0
        head = self.head
        while i < self.length:
            head.index = -1
            head = head.nxt
            i += 1

    def print(self, func):
        s = []

        def build_str(head):
            s.append("|{0}|{1} ".format(func(head), head.nxt_edge))
            return head
        self.map_ll(build_str)
        print("".join(s))


    @staticmethod
    def display_ll(ll):
        cytoscapeobj = initiate_cytospade()
        cytoscapeobj.user_zooming_enabled = False
        # Linked List Set the index
        nodes = []
        edges = []
        more_styles = []
        index = [0]
        if not ll.head:
            return 
        def set_index(llnode):
            llnode.index = index[0]
            index[0] += 1
            return llnode
        ll = ll.map_ll(set_index)
        if isinstance(ll.tail.nxt, LinkedListNode) and ll.tail.nxt.is_empty:
            set_index(ll.tail.nxt)

        def create_data(llnode):
            node = llnode._json()
            edge = llnode.nxt_edge._json()
            nodes.append(node)
            if llnode.has_style:
                more_styles.append(llnode.add_style())
            if llnode.nxt.index < llnode.index and len(edge) > 0:
                more_styles.append(LinkedList.add_curve(edge["data"]["id"]))
            if llnode and llnode.nxt:
                edges.append(edge)
            return llnode
        cytoscapeobj.set_style(initial_style + more_styles)
        ll = ll.map_ll(create_data)
        ll.reset_flags()
        ll.reset_index()
        data = {"nodes": nodes,
                "edges": edges}
        cytoscapeobj.graph.add_graph_from_json(data)
        cytoscapeobj.set_layout(name="preset")
        display(cytoscapeobj)
        return
