class Vertex:
    def __init__(self, index, value):
        self.index = index # Index corresponds to the index on adjacency matrix, also the id
        self.value = value
        self.flag = None

    def __str__(self):
        return str(self.value)

    def __hash__(self):
        return hash(str(self.value) + str(self.index))

    def set_flag(self, flag):
        if flag is None:
            print("Cannot set flag to None")
            return
        self.flag = flag

    def reset_flag(self):
        self.flag = None

    def get_flag(self):
        return self.flag

    def _json(self):
        if not self.value:
            return {"data": {"id": self.index, "class": "empty_node"}, "position": self.position}
        return {"data": {"id": self.index, "name": self.value}, "position": self.position}

    def __eq__(self, v):
        if not isinstance(v, Vertex):
            return False
        return v.value == self.value

    @property
    def position(self):
        return {}

    @staticmethod
    def get_vertices(V):
        i2v = {i: Vertex(i, v) for i, v in enumerate(V)}
        v2v = {i2v[i].value: i2v[i] for i in i2v}
        V = i2v.values()
        return V, i2v, v2v
    
class Edge:
    def __init__(self, edge, undirected=False, val=None):
        src, dst = tuple(edge)
        self.src = src
        self.dst = dst
        self.val = val
        self.undirected = undirected

    @staticmethod
    def get_edges(E, undirected, i2v, v2v):
        def switch_V(raw_edge):
            return Edge((v2v[raw_edge[0]], v2v[raw_edge[1]]), undirected)
        return set(map(switch_V, E))


    def __str__(self):
        if isinstance(self.val, bool):
            return "1" if self.val else "0"
        if not self.val:
            return "1"
        if self.dst.is_empty:
            return "/|"
        else:
            return str(self.val)

    def __hash__(self):
        return hash((self.src, self.dst))

    def _json(self):
        if self.src and self.dst:
            data = {"data": {"source": self.src.index, "target": self.dst.index, "id": "{0}-{1}".format(self.src.index, self.dst.index)}}
            return data
        return {}

    def verbose(self):
        if self.undirected:
            link = "-"
        else:
            link = "->"
        return "{0} {1} {2}".format(self.src, link, self.dst)

    def __eq__(self, o):
        if isinstance(o, Edge):
            if o.src and o.dst and self.src and self.dst:
                return True
            if o.src is None and self.src is None:
                return True
        return False

    @staticmethod
    def empty_edge():
        return Edge((None, None), val=False)