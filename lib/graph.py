"""
Graph Construction

## Adjacency Matrices

## Matrix Representations

"""
import sys
from nose.tools import assert_equal
import networkx as nx
from misc_utils import Vertex, Edge
from linked_list import LinkedListNode as LLNode

class Graph:
    def __init__(self, V, E, force_undirected=False):
        # We will build the graph based on the definition of edge type
        for e in E:
            break
 
        self.undirected = force_undirected

        self.V, i2v, v2v = Vertex.get_vertices(V)
        self.E = Edge.get_edges(E, self.undirected, i2v, v2v)
        self.lst_rep = self.build_adj(_AdjacencyList, self.undirected)
        self.mat_rep = self.build_adj(_AdjacencyMatrix, self.undirected)

        self.construct_nx(E)

    def construct_nx(self, E):
        return
        # self.nxG = ni.DiGraph()
        # self.nxG.add_edges_from(E)
        # pos = nx.spring_layout(G)
        # nx.draw_networkx_nodes(G, pos, cmap=plt.get_cmap('jet'), 
        #                     node_color = values, node_size = 500)
        # nx.draw_networkx_labels(G, pos)
        # nx.draw_networkx_edges(G, pos, edgelist=red_edges, edge_color='r', arrows=True)
        # nx.draw_networkx_edges(G, pos, edgelist=black_edges, arrows=False)
        # plt.show()
    
    def build_adj(self, cls, undirected):
        return cls(self.V, self.E, self.undirected)

    def traverse(self, func):
        # func takes in a graph and traverse
        return

    @staticmethod
    def printGraph(g):
        g.lst_rep.print_graph()
        return


    
class _AdjacencyMatrix:
    def __init__(self, V, E, undirected):
        
        self.V_size = len(V)
        self.V = V
        self.E = E
        self.undirected = undirected
        self.matrix = [[Edge.empty_edge() for _ in range(self.V_size)] for i in range(self.V_size)]
        for e in E:
            self._add_edge(e)
       
        
    def _add_edge(self, e):
        i = e.src.index
        j = e.dst.index
        self.matrix[i][j] = e
        if self.undirected:
            self.matrix[j][i] = e
    
    def remove_edge(self, i, j):
        self.matrix[i][j] = None

    def print_graph(self):
        for lst in self.matrix:
            print("|".join(map(str, lst)))

    @staticmethod
    def is_same(adj1, adj2):
        # Given two adjacency matrix, test they are same
        for i in range(len(adj1.matrix)):
            for j in range(len(adj1.matrix[i])):
                if not (len(adj2.matrix) > i and len(adj2.matrix[i]) > j and adj2.matrix[i][j] == adj1.matrix[i][j]):
                    return False
        return True


class _AdjacencyList:
    def __init__(self, V, E, undirected):
        self.V = V
        self.E = E
        # Implement adjacency list as linked list
        self.adjacencylists = [LLNode.empty() for _ in range(len(V))]
        self.undirected = undirected
        for e in E:            
            self._add_edge(e)

    def _add_edge(self, e):
        self.adjacencylists[e.src.index].append(e.dst)
        if self.undirected:
            self.adjacencylists[e.dst.index].append(e.src)
        

    def print_graph(self):
        i2v = {v.index:v for v in self.V}
        for i, l in enumerate(self.adjacencylists):
            print("|{}|->".format(i2v[i]), end=" ")
            print(l)
        return

    @staticmethod
    def is_same(adj1, adj2):
        # Given two adjacency list, test they are same
        i2v1 = {v.index:v for v in adj1.V}
        i2v2 = {v.value:v for v in adj2.V}
        if len(adj1.adjacencylists) != len(adj2.adjacencylists):
            return False
        for i in range(len(adj1.adjacencylists)):
            index2 = i2v2[i2v1[i].value].index
            set1 = adj1.adjacencylists[i].get_set()
            set2 = adj2.adjacencylists[index2].get_set()
            if set1 != set2:
                return False
        return True
        

    
def test_same_adj_lst(adj1, G_tuple, undirected):
    V, E = G_tuple
    adj2 = Graph(V, E, undirected).lst_rep
    print("Testing input adj_lst...")
    adj1.print_graph()
    print("Finish Priniting Adj_List...\n")
    if _AdjacencyList.is_same(adj1, adj2):
        print("Adjacency List Matches")
        
    else:
        print("Adjacency List Failed to Match \n")
        print("Expected adj_list")
        adj2.print_graph()
        assert_equal(True, False)

def test_same_adj_mat(adj1, G_tuple, undirected):
    V, E = G_tuple
    adj2 = Graph(V, E, undirected).mat_rep
    print("Testing input adjacency matrix...")
    adj1.print_graph()
    print("Finish Priniting Adjacency Matrix...\n")
    if _AdjacencyMatrix.is_same(adj1, adj2):
        print("Adjacency Matrix Matches")
        
    else:
        print("Adjacency Matrix Failed to Match \n")
        print("Expected Adjacency Matrix")
        adj2.print_graph()
        assert_equal(True, False)

V = set(range(1, 7))
E = set([(1, 2), (2, 5), (5, 4), (1, 4), (4, 2), (3, 5), (3, 6), (6, 6)])
directed_graph = Graph(V, E)
