"""Testing Utilities

Generally, we judge the quality of an algorithm by

1. Correctness
2. Time Complexity
3. Memory Complexity

This folder will provide testing class, mock values, and 
decorators for testing purposes.

"""
import time
from scipy import stats
import numpy as np
import copy
import sys
from typing import List, Tuple
from nose.tools import assert_equal
from collections.abc import Iterable
# Common Utilities
def timer(func, a):
    begin = time.time()
    output = func(*a)
    end = time.time()
    return [output, end - begin]

# MOCK Classes
class _DataType:

    @staticmethod
    def map_type(N):

        if isinstance(N, int):
            return _MockInt
        return None

    def magnifier(self, V):
        def it():
            nonlocal V
            t = copy.copy(V)
            V = V + self.DEFAULT_MULTIPLE
            yield t
        return it


class _MockInt(_DataType):
    DEFAULT_MULTIPLE = 1000

    def __init__(self, i):
        self.i = i
        return

    def __add__(self, v):
        time.sleep(0.01)
        if isinstance(v, int):
            self.i = self.i + v
        else:
            self.i = self.i + v.i
        return self

    def __float__(self):
        return float(self.i)

    def __radd__(self, v):
        time.sleep(0.01)
        if isinstance(v, int):
            self.i = self.i + v
        else:
            self.i = self.i + v.i
        return self

    def __str__(self):
        return self.i.__str__()

    def __repr__(self):
        return self.i.__repr__()

    def __mul__(self, N):
        self.i = self.i * N
        return self


class _MockList(_DataType):
    def __init__(self, lst):
        return


class _MockString(_DataType):
    def __init__(self, s):
        return


### Correctness Tests ###

def test_val(func, args, expected):
    """Testing if func will return expected value, expected is a primitive datatype

    Args:
        func (function): function to be tested
        args (list): list of arguments
        expected ([type]): Primitive Datatypes
    """
    result = func(*args)
    if isinstance(result, Iterable) and len(result) == 3:
        result, mutate_requirement, should_mutate = result
        assert_equal(result, expected)
        if not should_mutate:
            s = "n't"
        else:
            s = ""
        print("testing mutation requirement... solution should{} modify the linked list".format(s))
        assert_equal(mutate_requirement, True)
        print("Mutation Requirement passed")
    else:
        assert_equal(result, expected)
        print("Testing function {0}, with args {1}... Passed".format(func.__name__, args))
    print("\n")

def test_same_node(func, args, answer_func):
    """Return True function and answer function returns
    the same Node

    Args:
        func ([type]): [description]
        args ([type]): [description]
        answer_func ([type]): [description]
    """
    result = func(*args)
    args[0].reset_flags()
    args[0].reset_index()
    
    expected = answer_func(*args)
    if isinstance(result, Iterable) and len(result) == 3:
        result, mutate_requirement, should_mutate = result
        assert_equal(result, expected)
        if not should_mutate:
            s = "n't"
        else:
            s = ""
        print("testing mutation requirement... solution should{} modify the linked list".format(s))
        assert_equal(mutate_requirement, True)
        print("Mutation Requirement passed")



### Time Complexity Classes ###
"""Decorator Interface

Suppose we want to test function, product(a, b)

This case we answer with N, O, Om, and T
the decorator will return if the answer is correct
@runtime(N = N, O = O, Om = Om, T = T)


In this case we put constraint on the runtime.
@test_runtime(N = N, O = O, Om = Om, T = T)

2. Implement a Mock Operation for all the elementary
datatypes, the goal is to introduce a sleep
to extend the runtime

3. Implement: is_linear, is_constant, is_exponential, is_poly()
              is_log
"""


class TimeComplexity:
    def __init__(self, lst_inputs:List, lst_outputs_time:List[Tuple[List, List]]):
        lst_output, lst_time = zip(*lst_outputs_time)
        self.lst_inputs = np.array(lst_inputs)
        # Normalize lst_inputs


        self._calculate_regression()

    def _calculate_regression(self):
        return

    def _calculate_linear(self):
        _inputs = self.lst_inputs.astype(float)
        _inputs = _inputs / np.linalg.norm(_inputs)
        _, _, r_value, p_value, _ = stats.linregress(_inputs, self.lst_time)
        self.regression_r = {"linear": r_value**2}
        print(r_value**2)

    @property
    def is_linear(self):
        return self.get_complexity == "linear"

    @property
    def get_complexity(self):
        return max(self.regression_r.keys(), key=lambda x: self.regression_r[x])

    @property
    def is_constant(self):
        return False

    @property
    def is_exponential(self):
        return False

    @property
    def is_log(self):
        return False

    @property
    def is_polynomial(self):
        return False

    def complexity_match(self, O):
        print("Complexity Test Not Implemented")
        # print("Complexity Calculation is Correct O({})".format(O))
        # print("Complexity from Calculation is Incorrect.  From Calculation: {0} \n - Complexity from answered {1} ".format(
        #     complexity.get_complexity(), O))

    def _complexity_match(self, magn):
        # Test is Constant
        if magn == "1":
            return self.is_constant
        if magn == "N":
            return self.is_linear
        if magn == "log(N)":
            return self.is_log
        if magn == "2^N":
            return self.is_exponential
        if magn.startwith("N^"):
            return self.is_polynomial
        return False


class ComplexityTest:
    DEFAULT_SIZE = 200

    def __init__(self, a, i):
        # N is the index of the corresponding type
        a = list(a)
        N = a[i]
        # Match N_Type to corresponding magnifier
        self.MockType = _DataType.map_type(N)
        a[i] = self.MockType(N)
        self.i = i
        self.raw_input = a
        self.inputs_only = []

        if self.MockType is None:
            raise Exception(
                "Complexity Test Didn't Run, Type {} not found".format(type(N)))

    def run_complexity_test(self, func)->Tuple[List, List]:
        return ([], [(None, None)])

    def _run_complexity_test(self, func):
        # Replace the function N with corresponding functions
        magn = self.get_magnifier()
        _inputs = []
        for _ in range(ComplexityTest.DEFAULT_SIZE):
            v = next(magn())
            _inputs.append(v)

        outputs_time = [timer(func, i) for i in _inputs]
        return self.inputs_only, outputs_time

    def get_magnifier(self):
        magn = self.raw_input[self.i].magnifier(self.raw_input[self.i])
        self.inputs_only = []

        def it():
            v = next(magn())
            self.raw_input[self.i] = v
            self.inputs_only.append(v)
            yield tuple(self.raw_input)
        return it


def runtime(N, O, Om, T):
    def actual_decorator(func):
        def inner(*a, **kw):
            test = ComplexityTest(a, N)
            inputs, outputs_time = test.run_complexity_test(func)
            complexity = TimeComplexity(inputs, outputs_time)
            complexity.complexity_match(O)
            return func(*a, **kw)
        return inner
    return actual_decorator

def test_runtime(N, O, Om="", T=""):
    def actual_decorator(func):
        def inner(*a, **kw):
            print("Runtime Test Not Implemented")
            return func(*a, **kw)
        return inner
    return actual_decorator

def test_memory(N, O, Om="", T=""):
    def actual_decorator(func):
        def inner(*a, **kw):
            print("Memory Test Not Implemented")
            return func(*a, **kw)
        return inner
    return actual_decorator

### Memory Complexity Classes ###
